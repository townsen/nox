# Nox

[![Gem Version](http://img.shields.io/gem/v/nox.svg)][gem]

[gem]: https://rubygems.org/gems/nox

Nox is a command line tool (based on Thør) for doing things:

 * Run a micro-webserver to display GitHub markup in pages

## Installation

Install with:

    $ gem install nox

## Usage

### Command Line Tool

Type: `nox help` to get started

### Encoding

Internally all pages are transcoded to UTF-8 and served with content type
`text/html;charset=UTF-8`. The input files are assumed to be in the encoding specified by
your machine environment LANG variable, for example:

```bash
LANG=en_US.UTF-8
```

Set this prior to executing the server if your input files are in a different encoding.

## Development

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release` to create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

1. Fork it ( https://github.com/townsen/lux/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
