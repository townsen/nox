# Classes used by the GitHub server task
#
# Although we don't actually use this, without it: 'uninitialized constant GitHub'
require 'github/markup'

require 'html/pipeline'
require 'html/pipeline/rouge_filter'
require 'webrick'

module Nox
  class GitHubRenderer < WEBrick::HTTPServlet::AbstractServlet

    def initialize config, filename
      @filename = filename
      @pipeline = HTML::Pipeline.new [
        HTML::Pipeline::MarkdownFilter,
        HTML::Pipeline::RougeFilter
      ]
    end

    def do_GET req, rsp
      begin
        rsp.status = 200
        rsp['Content-Type'] = 'text/html'
        styles = []
        styles << %{<link crossorigin="anonymous" href="/style.css" media="all" rel="stylesheet"> }
        styles << %{ <style> body { padding: 25px 25px 25px 25px; } </style> }
        render = @pipeline.call File.read(@filename)
        rsp.body = <<-BODY
            <html>
            <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
              <head>#{styles.join('')}</head>
              <body>
                <article class="markdown-body">
        #{render[:output]}
                </article>
              </body>
            </html>
        BODY
      rescue
        rsp.status = 400
        rsp['Content-Type'] = 'text/plain'
        rsp.body = "HTML Pipeline failed: #{$!}"
      end
    end
  end

  class Styles < WEBrick::HTTPServlet::AbstractServlet
    def do_GET req, rsp
      begin
        rsp.status = 200
        rsp['Content-Type'] = 'text/css'
        rsp.body = ['normalize', 'github', 'override'].map do |style|
          File.read(File.absolute_path("../../#{style}.css", File.dirname(__FILE__)))
        end.join("\n")
      end
    end
  end
end
