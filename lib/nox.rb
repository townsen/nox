require 'nox/version'
require 'nox/ghserver'

require 'highline/import'
require 'thor'
require 'webrick'

# The Thor subclass containing useful tasks 
#
class Nox::App < Thor

  desc "version", "Show the version"
  def version
    puts "Version #{Nox::VERSION}"
  end

  desc "ghserver [PATH]", "Start a webserver that does GitHub markup"
  method_option :port, type: :numeric, aliases: '-p', default: 8019, desc: 'Port'
  def ghserver(path=Dir.getwd)

    Encoding.default_internal = 'utf-8'
    WEBrick::HTTPServlet::FileHandler.add_handler('md', ::Nox::GitHubRenderer)
    WEBrick::HTTPServlet::FileHandler.add_handler('markdown', ::Nox::GitHubRenderer)
    server = WEBrick::HTTPServer.new DocumentRoot: path, BindAddress: '0.0.0.0', Port: options.port
    server.mount '/style.css', ::Nox::Styles

    ['INT', 'TERM'].each {|signal| trap(signal) { server.shutdown } }
    server.start

  end

  private

  def die msg, rc = 1
    HighLine.say HighLine.color(msg, HighLine::RED)
    exit(rc)
  end

end

# vim: ft=ruby sts=2 sw=2 ts=8
