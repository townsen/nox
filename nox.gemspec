# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'nox/version'

Gem::Specification.new do |spec|
  spec.name          = "nox"
  spec.version       = Nox::VERSION
  spec.authors       = ["Nick Townsend"]
  spec.email         = ["nick.townsend@mac.com"]

  spec.summary       = %q{Handy utilities for coding}
  spec.homepage      = "https://github.com/townsen/nox"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "thor", "~> 0.19"
  spec.add_runtime_dependency "highline", "~> 1.7"
  spec.add_runtime_dependency "html-pipeline"
  spec.add_runtime_dependency "html-pipeline-rouge_filter"
  spec.add_runtime_dependency "github-markup", "~> 1.4.0"
  spec.add_runtime_dependency "github-markdown", "~> 0.6.8"
  spec.add_runtime_dependency "rake", "~> 10.0"
  spec.add_runtime_dependency "rouge", "~> 1.9.0", "!= 1.9.1"

  spec.add_development_dependency "bundler", "~> 1.9"
end
